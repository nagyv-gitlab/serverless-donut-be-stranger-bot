'use strict';

const Main = require('./index.js')

describe('Main', () => {
  it('fails if members file is not given', () => {
    expect(Main).toThrow()
  })
  it('fails if members file is invalid', () => {
    expect(() => Main('mocks/members_wrong.json')).toThrow()
  })
  it('returns JSON is Slack format', () => {
    const resp = JSON.parse(Main('mocks/members.json'))
    expect(resp.blocks).toBeTruthy()
    // Last block holds the pairing
    expect(resp.blocks.slice(-1)[0].text.text).toMatch(/<@\w+> - <@\w+>/)
  })
})
