'use strict';

const Donut = require('./donut.js')

describe('Pairing', () => {
    it('returns empty string if 0-1 people are provided', () => {
        expect(Donut.pairing([])).toEqual('')
        expect(Donut.pairing(['lonely'])).toEqual('')
    })
    it('returns pairs of @users', () => {
        const resp = Donut.pairing(['you', 'me', 'him', 'her'])
        expect(resp.split('\n')).toHaveLength(2)
        resp.split('\n').map((pair) => {
            expect(pair).toMatch(/<@\w+> - <@\w+>/)
        })
    })
    it('works with uneven users by dropping one', () => {
        const resp = Donut.pairing(['you', 'me', 'him', 'her', 'X'])
        expect(resp.split('\n')).toHaveLength(2)
    })
})
