'use strict';

function pairing(people) {
    if(people.length % 2  == 1) {
        people.splice(Math.floor(Math.random() * people.length), 1)
    }

    let pair = []
    while(people.length >0) {
        let itemIdx = Math.floor(Math.random() * people.length)
        let p1 = people[itemIdx].trim()
        people.splice(itemIdx, 1)

        itemIdx = Math.floor(Math.random() * people.length)
        let p2 = people[itemIdx].trim()
        people.splice(itemIdx, 1)
        pair.push(`<@${p1}> - <@${p2}>`)
    }
    return pair.join('\n')
}

module.exports = {
    pairing
}
