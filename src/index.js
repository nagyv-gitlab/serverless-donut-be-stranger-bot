'use strict';

const Donut = require('./donut.js')
const path = require('path');
// userIds: PTO ninja, donut bot, PTO ninja new, John-Mason Shackelford, Jason Yavorska
const botUserIds = ['U4YB023N1', 'UQBAE2YKT', 'U019WPGD77V', 'UUUH29Q95', 'UBGLWFV5K']

function buildResponse(pairs) {
    return {
        "text": "Hmmm, whom should you invite for a coffee? I've made my matches!",
        blocks: [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "*Hello people!*"
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "Please, meet for your next coffee!"
                }
            },
            {
                "type": "section",
                "text": {
                    "text": pairs,
                    "type": "mrkdwn"
                }
            }
        ]
    }
}

function index(membersFile) {
    const members = readfile(membersFile)
    if(!members) {
        throw new Error('No members found')
    }

    const pairs = Donut.pairing(members)
    return JSON.stringify(buildResponse(pairs))
}

function readfile(membersFile) {
    if(!membersFile) {
        throw new Error('Requires a members file')
    }
    return require(path.join(process.cwd(), membersFile)).members.filter( member => botUserIds.indexOf(member) == -1 )
}

if (require.main === module) {
    try {
        if(process.argv.length != 3) {
            throw new Error('Expected a single argument, the members file')
        }

        console.log(index(process.argv[2]))
    } catch(e) {
        console.error(e.message)
        process.exit(1)
    }
}

module.exports = index
