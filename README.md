# GitLab simple donut be stranger bot

This is a simple CI/CD job to randomly match GitLab users in a Slack channel.

## How to use

1. Create a new Slack incoming webhook at https://api.slack.com/apps/AUHCAMU9F/incoming-webhooks
1. Start a new Scheduled run at https://gitlab.com/nagyv-gitlab/serverless-donut-be-stranger-bot/
    1. Set the `SLACK_WEBHOOK_URL` variable to the webhook url created above
    1. Set the `DONUT_USERS` variable to a comma separated list of Slack usernames to match

## Possible future developnents

Contributions are welcome!

- Derive the GitLab handles from the channel's group members.
- Make the matching message beautiful and fun.
